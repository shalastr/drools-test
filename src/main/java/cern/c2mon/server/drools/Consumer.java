package cern.c2mon.server.drools;

/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import cern.c2mon.shared.client.alarm.AlarmValue;
import cern.c2mon.shared.client.serializer.TransferTagSerializer;
import cern.c2mon.shared.client.tag.TransferTagImpl;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

  @Autowired
  KieSession kieSession;

  @JmsListener(destination = "c2mon.rules.result")
  public void receive2(String text) {
    System.out.println("TEST RULE RESULT: " +text);
  }

  @JmsListener(destination = "c2mon.client.tag.*")
  public void receiveQueueTag(String text) {
    TransferTagImpl tag = TransferTagSerializer.fromJson(text, TransferTagImpl.class);
    kieSession.insert(tag);
    for (AlarmValue alarmValue : tag.getAlarmValues()) {
      if (alarmValue.isActive()) {
        kieSession.insert(alarmValue);
      }
    }
  }

}